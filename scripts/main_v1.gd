# grid data structure is defined in res://scripts/autoload/grid_manager.gd!

extends Spatial

const ray_length = 1000
const v_up = Vector3(0,1,0)
const v_front = Vector3(0,0,1)

onready var gm = get_node( "/root/grid_manager" )

onready var cam = $cam_pivot/cam
onready var current_player = $board.DEFAULT_MATID
onready var ball_tmpl = load( "res://models/ball.tscn" )
onready var ball_mat = load( "res://materials/ball.material" )
onready var ball_mats = []

var ray_process = false
var drag_mouse_pos = Vector2()
var mouse_pos = Vector2()
var dragged = null
var drag_init = null
var button_hover = null
var pusher_mode = 0
var fallen_balls = []

var board_folder = "res://models/boards/"
var board_prefix = "board_"
var board_range = [1,8]
var board_default = 3
var board_current = board_default
var board_list = []

var previous_ball_config = null

####################
# board management #
####################

func load_current_board():
	
	if $board and board_current != -1 and board_current < len( board_list ):
		var mats = $board.mat_button_list
		var decoys = $board.decoy_mats
		remove_child( $board )
		var b = load( board_list[ board_current ] ).instance()
		add_child( b )
		b.mat_button_list = mats
		b.decoy_mats = decoys
		
		# grid related
		$grid_display.grid = gm.generate_grid( board_range[0] + board_current )
		$grid_display.synchronise()

################
# system calls #
################

func _ready():
	
	# loading boards
	for bi in range( board_range[0], board_range[1] + 1 ):
		var n = str( bi )
		while len(n) < 3:
			n = '0' + n
		n = board_folder + board_prefix + n + '.tscn'
		var file = File.new()
		if file.file_exists(n):
			board_list.append( n )
	load_current_board()
	
# warning-ignore:return_value_discarded
	$menu.connect( "menu_open", self, "menu_open" )
# warning-ignore:return_value_discarded
	$menu.connect( "menu_play", self, "menu_play" )
# warning-ignore:return_value_discarded
	$menu.connect( "menu_edit", self, "menu_edit" )
# warning-ignore:return_value_discarded
	$menu.connect( "menu_player", self, "menu_player" )
# warning-ignore:return_value_discarded
	$menu.connect( "menu_clear", self, "menu_clear" )
# warning-ignore:return_value_discarded
	$menu.connect( "menu_board", self, "menu_board" )
	$board/buttons.visible = false
	
	for i in range( 0, len( $board.mat_button_list ) ):
		var color = $board.get_mat_albedo( i )
		# adding a ball material for the player
		var bt = ball_mat.duplicate()
		bt.set_shader_param( "albedo", color )
		ball_mats.append( bt )
		# adapting UI
		$menu.set_player_color( i, color )
	
	prepare_fallen()
	disable_pusher()

# warning-ignore:unused_argument
func _process(delta):
	
	#tracking balls
	var to_trash = []
	for c in $balls.get_children():
		if c.translation.y < 0:
			to_trash.append( c )
	for t in to_trash:
		add_fallen( t )
		$balls.remove_child( t )
	
	for fbs in fallen_balls:
		for fb in fbs:
			var newp = fb.mesh.translation + ( fb.pos - fb.mesh.translation ) * delta * 3
#			newp = newp.normalized() * fb.radius
			fb.mesh.translation = newp

# warning-ignore:unused_argument
func _physics_process(delta):
	
	mouse_pos = get_viewport().get_mouse_position()
	
	if ray_process:
		ray_process = false
		var ray = $hit_rc
		if ray.is_colliding() and ray.get_collider() is RigidBody:
			dragged = ray.get_collider()
			if dragged.has_method( "highlight" ):
				dragged.highlight(true)
			drag_init = dragged.global_transform.origin
			drag_mouse_pos = mouse_pos
			enable_pusher( drag_init )
			$pusher.visible = false
		else:
			release_dragged()
	
	elif dragged != null:

		var do = dragged.global_transform.origin
		
		var cam_pos = cam.project_ray_origin(mouse_pos)
		var cam_norm = cam.project_ray_normal(mouse_pos)
		var mult = cam_norm.dot( Vector3(0,-1,0) )
		var displace = ( cam_norm * (cam_pos.y - drag_init.y) ) / mult
		var dp = cam_pos + displace
		
		var vl = (dp-do).length()
		if vl > 0.05:
			orient_pusher( dp )
			$pusher.visible = true
		if vl >= 1:
			pusher_mode = 1
		if pusher_mode == 1:
			move_pusher( dp )

#####################
# physics management #
#####################

func release_dragged():
	if dragged != null and dragged.has_method( "highlight" ):
		dragged.highlight(false)
	disable_pusher()
	dragged = null

func update_ray( ray, v2 ):
	ray.translation = cam.project_ray_origin(v2)
	ray.cast_to = ray.translation + cam.project_ray_normal(v2) * ray_length

func disable_pusher():
	pusher_mode = 0
	$pusher.visible = false
	$pusher.set_collision_layer_bit( 0, false )
	$pusher.set_collision_mask_bit( 0, false )

func enable_pusher( v3 ):
	pusher_mode = 0
	$pusher.visible = true
	$pusher.set_collision_layer_bit( 0, true )
	$pusher.set_collision_mask_bit( 0, true )
	$pusher.translation = v3
	if dragged != null and dragged.has_method( "get_albedo" ):
		$pusher.set_mat_albedo( dragged.get_albedo() )

func orient_pusher( v3 ):
	$pusher.look_at( v3, v_up )

func move_pusher( v3 ):
	var offset = $pusher.global_transform.basis.xform( v_front ) * -1
	$pusher.translation += ( ( v3 - offset ) - $pusher.translation ) * 0.3

#####################
# events management #
#####################

func _input(event):
	
	var mhover = $menu.is_hovered()
	
	if mhover:
		button_hover = null
		$board.hover_button( null )
		ray_process = false
		$cam_pivot.enabled = false
		return
	else:
		$cam_pivot.enabled = true
	
	if event is InputEventMouseButton:
		
		if button_hover != null and event.button_index == 1 and event.pressed:
			$board.click_button( button_hover, current_player )
			sync_player()
		
		elif event.pressed and event.button_index == 1:
			ray_process = true
			update_ray( $hit_rc, event.position )
		
		elif not event.pressed and event.button_index == 1:
			if dragged != null:
				release_dragged()
	
	elif event is InputEventMouseMotion and $board/buttons.visible:
		update_ray( $button_rc, event.position )
		if $button_rc.is_colliding():
			var b = $button_rc.get_collider()
			button_hover = b
			$board.hover_button( b, current_player )
		else:
			button_hover = null
			$board.hover_button( null )

func sync_player():
	var allb = $board.get_selected_positions()
	for i in range( 0, len(allb) ):
		$menu.set_player_count( i, len( allb[i] ) )

####################
# balls management #
####################

func clear_balls( only_static = false ):
	if only_static:
		var sts = []
		for c in $balls.get_children():
			if c.mode == RigidBody.MODE_STATIC:
				sts.append(c)
		for s in sts:
			$balls.remove_child( s )
	else:
		if $balls.get_child_count() > 0:
			previous_ball_config = []
		while $balls.get_child_count() > 0:
			var b = $balls.get_child(0)
			previous_ball_config.append( { 
				'time_offset': b.get_time_offset(),
				'rotation': b.rotation
				} )
			$balls.remove_child( b )

func generate_balls( preview = false ):
	
	var allb = $board.get_selected_positions()
	
	var new_count = 0
	for plb in allb:
		new_count += len( plb )
	
	var rand_toffset = not len( previous_ball_config ) == new_count
	
	var bi = 0
	for i in range( 0, len( allb ) ):
		var plb = allb[i]
		for pos in plb:
			var b = ball_tmpl.instance()
			if preview:
				b.mode = RigidBody.MODE_STATIC
			$balls.add_child( b )
			b.set_mat( ball_mats[ i ] )
			if rand_toffset:
				b.set_time_offset( rand_range(0,0.25) )
			else:
				b.set_time_offset( previous_ball_config[bi].time_offset )
				b.rotation = previous_ball_config[bi].rotation
			b.global_transform.origin = pos + Vector3( 0,1.5,0 )
			bi += 1

func prepare_fallen():
	fallen_balls = []
# warning-ignore:unused_variable
	for i in range( 0, len( $board.mat_button_list ) ):
		fallen_balls.append( [] )

func clear_fallen():
	for fbs in fallen_balls:
		for fb in fbs:
			remove_child( fb.mesh )
	prepare_fallen()

func add_fallen( ball ):
	
	var mt = ball.get_mat()
	
	for i in range( 0, len(ball_mats) ):
		
		if mt == ball_mats[i]:
			var m = ball.get_mesh()
			var rot = ball.rotation
			var pos = m.global_transform.origin
			m.get_parent().remove_child( m )
			add_child( m )
			# rendering position
			var offset = TAU / len( fallen_balls ) * i
			var b = $board.get_node( $board.grid[ len( $board.grid ) - 1 ][0][0].path )
			var radius = b.global_transform.origin.length() + 5
			var a = offset + PI * len(fallen_balls[i]) * 0.05
			var target = Vector3( cos( a ) * radius, -2.2 * i, sin( a ) * radius )
			m.global_transform.origin = pos
			m.rotation = rot
			fallen_balls[i].append( { 
				'mesh': m, 
				'radius': radius,
				'pos': target 
				} )

######################
# signals management #
######################

func menu_open():
	pass

func menu_play():
	$board/buttons.visible = false
	clear_balls( true )
	generate_balls()

func menu_edit():
	$board/buttons.visible = false
	clear_balls()
	clear_fallen()
	generate_balls( true )

func menu_player( id ):
	$board/buttons.visible = true
	clear_balls()
	if id != null:
		current_player = id

func menu_clear():
	$board.reset( current_player )
	sync_player()

func menu_board( way ):
	
	$board/buttons.visible = false
	clear_balls()
	generate_balls( true )
	
	if way != null:
		board_current += way
		if board_current < 0:
			board_current = 0
		if board_current >= len( board_list ):
			board_current = len( board_list ) - 1
		
		$menu.set_board_operator( board_current != 0, board_current != len( board_list ) - 1 )
		load_current_board()
		sync_player()
		$board/buttons.visible = false
