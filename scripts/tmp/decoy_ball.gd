extends Spatial

export (float,0,10) var _speed_x = 0.06 setget speed_x
export (float,0,10) var _speed_y = 1 setget speed_y
export (Color) var _external_color = Color(1,1,1,1) setget external_color
export (Color) var _internal_color = Color(0,0,0,1) setget internal_color
export (float,0,2) var rotation_speed = 1

onready var external_mat = $mesh.material_override
onready var internal_mat = $mesh.material_override.next_pass

var rotation_vec = Vector3()

func apply_speed():
	if external_mat == null:
		return
	var s = external_mat.get_shader_param( 'speed' )
	s.x = _speed_x
	s.y = _speed_y
	external_mat.set_shader_param( 'speed', s )
	internal_mat.set_shader_param( 'speed', s )

func speed_x( f = null ):
	if f == null:
		return _speed_x
	_speed_x = f
	apply_speed()
	return _speed_x
	
func speed_y( f = null ):
	if f == null:
		return _speed_y
	_speed_y = f
	apply_speed()
	return _speed_y
	
func external_color( c = null ):
	if c == null:
		return _external_color
	_external_color = c
	if external_mat != null:
		external_mat.set_shader_param( 'albedo', _external_color )
		return _external_color
	return null
	
func internal_color( c = null ):
	if c == null:
		return _internal_color
	_internal_color = c
	if internal_mat != null:
		internal_mat.set_shader_param( 'albedo', _internal_color )
		return _internal_color
	return null

func get_material():
	return $mesh.material_override

func set_material( m ):
	$mesh.material_override = m
	external_mat = $mesh.material_override
	internal_mat = $mesh.material_override.next_pass

func _ready():
	apply_speed()
	external_color( _external_color )
	internal_color( _internal_color )
	rotation_vec = Vector3( rand_range(-1,1), 0, rand_range(-1,1) )
	rotation_vec.normalized()

func _process(delta):
	rotation += rotation_vec * rotation_speed * delta
