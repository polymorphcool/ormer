extends Node2D

const offset_closed = Vector2( -105, -105 )
const offset_opened = Vector2( -175, -105 )
const offset_conf = Vector2( -245, -105 )

signal menu_open
signal menu_edit
signal menu_clear
signal menu_play
signal menu_player
signal menu_board
signal menu_config_save
signal menu_config_reset

onready var gm = get_node("/root/grid_manager")
onready var pm = get_node("/root/player_manager")
onready var player_bts = [ $main/edit/player/p0, $main/edit/player/p1, $main/edit/player/p2 ]

export (bool) var _synchronise_style = false setget synchronise_style

var hexa_bts = []
var offset = offset_closed

# warning-ignore:unused_argument
func synchronise_style( b ):
	_synchronise_style = false
	if _synchronise_style:
		var tmpl = load( "res://models/2d/hexa_button.tscn" ).instance()
		for c in get_children():
			if c.has_method( "cp_styles" ):
				c.cp_styles( tmpl )

func is_hovered():
	for h in hexa_bts:
		if h.hovered:
			return true
	return false

func collect_hexa( root ):
	for c in root.get_children():
		if c.has_method( "hexa_connect" ):
			hexa_bts.append( c )
			collect_hexa( c )

func _ready():
	for c in get_children():
		if c.has_method( "hexa_connect" ):
			c.hexa_connect( self, "hexa_pressed" )
	collect_hexa( self )

# warning-ignore:unused_argument
func _process(delta):
	
	var vps = get_viewport().size
	var mp = vps + offset
	$main.position += (mp-$main.position) * 5 * delta
	
	var np = Vector2( 20, vps.y - 65 )
	$notice.position += (np-$notice.position) * 20 * delta

func set_player_color( pid, c ):

	if pid < 0 or pid >= len( player_bts ):
		return
	
	var p = player_bts[ pid ].get_node( "color" )
	p.normal_style.border = Color( c.r, c.g, c.b, p.normal_style.border.a )
	p.normal_style.bg = Color( c.r, c.g, c.b, p.normal_style.bg.a )
	p.normal_style.txt = Color( 1-c.r, 1-c.g, 1-c.b, p.normal_style.txt.a )
	p.start_style.border = p.normal_style.border
	p.start_style.bg = p.normal_style.bg
	p.start_style.txt = p.normal_style.txt

func set_board_operator( minus_enabled, plus_enabled ):
	$main/edit/board/minus.disable( not minus_enabled )
	$main/edit/board/plus.disable( not plus_enabled )

func swap_player( i ):
	
	for bi in range( 0, len(player_bts) ):
		var b = player_bts[bi]
		if bi != i:
			b.selectable = true
			b.selected = false
			b.display_submenu( false )
			b.select( false )
		elif b.selectable:
			b.selectable = false
			b.selected = true
			b.display_submenu( true )
			b.select( true )
	pass

func sync_players():
	
	for btn in player_bts:
		btn.visible = false
	
	for pi in range( 0, len(pm.players)):
		if pi >= len(player_bts):
			return
		var pbtn = player_bts[pi]
		var color = player_bts[pi].get_node( 'color' )
		var player = pm.players[pi]
		color.start_style.bg = player.color
		color.start_style.txt = player.txt_color
		color.normal_style.bg = player.color
		color.normal_style.txt = player.txt_color

func set_player_count():
	for pi in range( 0, len(pm.players) ):
		player_bts[pi].get_node('color').text( str( gm.get_cells_count( gm.active_grid, pi ) ) )

func signal_player():
	var i = 0
	for p in player_bts:
		if p.selected:
			emit_signal( "menu_player", i )
		i += 1

func hexa_pressed( button ):
	
	print( button )
	
	if button == $main:
		button.disable( true )
		button.display_submenu( true )
		emit_signal( "menu_open" )
		offset = offset_opened
	
	elif button == $main/play:
		$main.disable( false )
		$main.display_submenu( false )
		emit_signal( "menu_play" )
		offset = offset_closed
	
	###############
	# EDIT BUTTON #
	###############
	
	elif button == $main/edit:
		$main.disable( true )
		if button._text == "EDIT":
			$main/play.disable( true )
			$main/conf.disable( true )
			button.text( "OK" )
			button.display_submenu( true )
			$main/edit/board.selectable = true
			$main/edit/player.selectable = true
		else:
			$main/play.disable( false )
			$main/conf.disable( false )
			button.text( "EDIT" )
			button.display_submenu( false )
			$main/edit/board.select( false )
			$main/edit/board.display_submenu( false )
			$main/edit/player.select( false )
			$main/edit/player.display_submenu( false )
		emit_signal( "menu_edit" )
	
	#################
	# PLAYER BUTTON #
	#################
	
	elif button == $main/edit/player:
		if button.selectable:
			button.selectable = false
			button.display_submenu( true )
			$main/edit/board.selectable = true
			$main/edit/board.select( false )
			$main/edit/board.display_submenu( false )
			if pm.current_player == -1:
				pm.current_player = 0
			swap_player( pm.current_player )
			signal_player()
	
	elif button == $main/edit/player/clear:
		emit_signal( "menu_clear" )
		set_player_count()
	
	elif button == $main/edit/player/p0:
		swap_player(0)
		signal_player()
		
	elif button == $main/edit/player/p1:
		swap_player(1)
		signal_player()
		
	elif button == $main/edit/player/p2:
		swap_player(2)
		signal_player()
	
	################
	# BOARD BUTTON #
	################
	
	elif button == $main/edit/board:
		if button.selectable:
			button.selectable = false
			button.display_submenu( true )
			$main/edit/player.selectable = true
			$main/edit/player.select( false )
			$main/edit/player.display_submenu( false )
			emit_signal( "menu_board", null )
	
	elif button == $main/edit/board/minus:
		emit_signal( "menu_board", -1 )
	
	elif button == $main/edit/board/plus:
		emit_signal( "menu_board", 1 )
	
	###############
	# CONF BUTTON #
	###############
	
	elif button == $main/conf:
		$main.disable( true )
		if button._text == "CONF": 
			$main/play.disable( true )
			$main/edit.disable( true )
			button.text( "OK" )
			button.display_submenu( true )
			offset = offset_conf
		else:
			$main/play.disable( false )
			$main/edit.disable( false )
			button.text( "CONF" )
			button.display_submenu( false )
			offset = offset_opened
	
	elif button == $main/conf/save:
		emit_signal( "menu_config_save" )
	
	elif button == $main/conf/reset:
		emit_signal( "menu_config_reset" )
