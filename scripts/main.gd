extends Spatial

const grid_path = "user://ormer_default_grid.json"
const player_path = "user://ormer_player.json"
const abalone_grid_path = "res://json/abalone_grid.json"

const board_folder = "res://models/boards/"
const board_prefix = "board_"
const board_range = [1,8]
const board_default = 4

onready var gm = get_node("/root/grid_manager")
onready var pm = get_node("/root/player_manager")

var board_current = board_default

func load_board():
	
	gm.active_selection = gm.new_selection()
#	gm.active_selection.max = 5
#	gm.active_selection.inline = false

	if gm.active_grid.depth != board_current:
		gm.active_grid = gm.generate_grid( board_current )
		
	$board.synchronise()
	$grid_display.synchronise()

func _ready():
	
	# loading jsons & board resources
	if not pm.load( player_path ):
		pm.add_player( Color( 1.0,1.0,1.0,1.0 ) )
		pm.add_player( Color( 0.0,0.0,0.0,1.0 ) )
		pm.add_player( Color( 1.0,0.0,0.0,1.0 ) )
		pm.save( player_path )
	var default_grid = gm.load( grid_path )
	if default_grid == null:
		default_grid = gm.load( abalone_grid_path )
		if default_grid == null:
			var g = gm.generate_grid( board_default )
			gm.active_grid = g
		else:
			gm.active_grid = default_grid
		gm.save( grid_path, gm.active_grid )
	else:
		gm.active_grid = default_grid
	
	# loading default board
	load_board()
	# preparing board
	show_pastilles(false)
	$board.show_decoys( false )
	# synchronising balls
	$balls.synchronise()
	# adjusting UI
	$menu.sync_players()
	$menu.set_player_count()
	$grid_display.visible = false
	
	# linking signals
# warning-ignore:return_value_discarded
	$menu.connect( "menu_open", self, "menu_open" )
# warning-ignore:return_value_discarded
	$menu.connect( "menu_play", self, "menu_play" )
# warning-ignore:return_value_discarded
	$menu.connect( "menu_edit", self, "menu_edit" )
# warning-ignore:return_value_discarded
	$menu.connect( "menu_player", self, "menu_player" )
# warning-ignore:return_value_discarded
	$menu.connect( "menu_clear", self, "menu_clear" )
# warning-ignore:return_value_discarded
	$menu.connect( "menu_board", self, "menu_board" )
# warning-ignore:return_value_discarded
	$menu.connect( "menu_config_save", self, "save_config" )
# warning-ignore:return_value_discarded
	$menu.connect( "menu_config_reset", self, "reset_config" )
# warning-ignore:return_value_discarded
	$interaction.connect( "pastille_hover", $board, "pastille_hover" )
# warning-ignore:return_value_discarded
	$interaction.connect( "pastille_click", self, "pastille_click" )

func show_pastilles( b ):
	$board.show_pastilles( b )
	$interaction.pastilles_enabled = b

func menu_open():
	$grid_display.visible = true
	pass

func menu_play():
	show_pastilles( false )
	$board.show_decoys( false )
	$balls.synchronise()
	$grid_display.visible = false

func menu_edit():
	# canceling previous selection
	gm.active_selection = gm.new_selection()
	show_pastilles( false )
	$board.show_decoys( true )
	$balls.purge()

func menu_player( id ):
	
	pm.current_player = id
	$board.pastille_hover( null )
	show_pastilles( true )

func menu_clear():
	gm.reset_cells( gm.active_grid, pm.current_player )
	$board.synchronise()
	$grid_display.synchronise()

func menu_board( way ):
	
	show_pastilles( false )
	
	if way != null:
		board_current += way
		if board_current < board_range[0]:
			board_current = 0
		if board_current > board_range[1]:
			board_current = board_range[1]
		
		$menu.set_board_operator( board_current != board_range[0], board_current != board_range[1] )
		var previous_grid = gm.active_grid
		gm.active_grid = gm.generate_grid( board_current )
		gm.transfer_player( previous_grid, gm.active_grid )
		load_board()
		show_pastilles( false )

func pastille_click( b ):
	
	var id = $board.get_pastille_id( b )
	var cell = gm.get_cell_by_id( gm.active_grid, id )
	if cell.player != pm.current_player:
		cell.player = pm.current_player
	else:
		cell.player = -1
	
	$menu.set_player_count()
	$board.pastille_click( b )
	$grid_display.synchronise()

func save_config():
	gm.save( grid_path, gm.active_grid )
	pm.save( player_path )

func reset_config():
	
	var g = gm.load( abalone_grid_path )
	if g != null:
		gm.active_grid = g
		$balls.purge()
		load_board()
	
