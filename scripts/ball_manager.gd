extends Node

const ball_path = "res://models/ball.tscn"

onready var gm = get_node("/root/grid_manager")
onready var pm = get_node("/root/player_manager")
onready var ball_tmpl = load( ball_path )

export (float, 0, 10 ) var init_ball_y = 0


func _ready():
	pass

func purge():
	# remove all balls
	while get_child_count() > 0:
		remove_child( get_child( 0 ) )

func synchronise():
	
	# remove all balls
	purge()
	
	if gm.active_grid == null or gm.active_grid.cell_num == 0:
		return
	
	var c = gm.get_cell_by_id( gm.active_grid, 0 )
	if not 'pastille' in c:
		if OS.is_debug_build():
			printerr("ball_manager::synchronise : missing key 'pastille' in cells!")
		return
	
	for i in range( 0, gm.active_grid.cell_num ):
		var cell = gm.get_cell_by_id( gm.active_grid, i )
		if cell.player != -1:
			var player = pm.get_player( cell.player )
			var b = ball_tmpl.instance()
			b.translation = cell.pastille.global_transform.origin + Vector3(0,init_ball_y,0)
			b.rotation = cell.pastille.get_decoy_rotation()
			b.set_mat( player.mat_ball )
			add_child( b )
			
