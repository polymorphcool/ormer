extends StaticBody

# warning-ignore:unused_class_variable
export (int) var level = -1
# warning-ignore:unused_class_variable
export (int) var corner = -1
# warning-ignore:unused_class_variable
export (int) var id = -1
# warning-ignore:unused_class_variable
export (Material) var mat = null

var selected = false

# editor function, DO NOT USE ingame
func scale_pastille( v3 ):
	$pastille_coll.scale = v3
	$pastille_mesh.scale = v3

func decoy_enable( b ):
	$decoy_coll.disabled = !b
	$decoy_mesh.visible = b

func decoy_enabled():
	return !$decoy_coll.disabled

func pastille_material( mat = null ):
	if mat == null:
		return $pastille_mesh.material_override
	$pastille_mesh.material_override = mat
	return mat
	
func decoy_material( mat = null ):
	if mat == null:
		return $decoy_mesh.get_material()
	$decoy_mesh.set_material( mat )
	return mat

func show_pastille( b ):
	$pastille_coll.disabled = !b
	$pastille_mesh.visible = b

func show_decoy( b ):
	$decoy_coll.disabled = !b
	$decoy_mesh.visible = b

func get_decoy_rotation():
	return $decoy_mesh.global_transform.basis.get_euler()
