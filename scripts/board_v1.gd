extends Spatial

const DEFAULT_MATID = 0

export (int,0,20) var depth = 2
export (float,0,10) var radius = 20
export (float,0,10) var gap = 5
export (float,0,10) var radius2 = 5
export (float,0,10) var height2 = 5
export (float,0,10) var radius3 = 5
export (float,0,10) var height3 = 5
export (bool) var generate = false setget _generate

export (Array) var grid = []

var slot = null
var button = null
var prev_hover = null

const slot_path = "res://models/slot.tscn"
const button_path = "res://models/button.tscn"

onready var mat_button_default = load( "res://materials/stripes.material" )
# warning-ignore:unused_class_variable
onready var mat_button_selected = load( "res://materials/circles.material" )
# warning-ignore:unused_class_variable
onready var decoy_material = load( "res://materials/decoy_ball.material" )

onready var mat_button_list = [
	mat_button_selected.duplicate(),
	mat_button_selected.duplicate()
]

onready var decoy_mats = [
	decoy_material.duplicate(),
	decoy_material.duplicate()
]

func do_generate():
	
	while( get_child_count() > 0 ):
		remove_child( get_child(0) )

	slot = load(slot_path)
	button = load(button_path)
	
	var bi = button.instance()
	bi.scale_pastille( Vector3( radius3, height3, radius3 ) )
	var pb = PackedScene.new()
	var result = pb.pack(bi)
	if result == OK:
# warning-ignore:return_value_discarded
		ResourceSaver.save(button_path, pb)
		button = load(button_path)
	
	grid = []
	
	var r_slot = ( radius * sin(PI/3) + gap ) * 0.5
	var r_button = 0
	var id_slot = 0
	
	for lvl in range( 0, depth + 1 ):
		
		if lvl > -1:
			
			var a_slot = PI / 6
			var a_button = PI / 6
			var rot_slot = 0
			
			if lvl == 0:
				add_button( button, lvl, 0, 0 )
			
			for i in range( 0, 6 ):
				
				var div = lvl * 4 + 2
				var start = Vector3( cos(a_slot), 0, sin(a_slot) ) * r_slot
				a_slot += PI / 3
				var end = Vector3( cos(a_slot), 0, sin(a_slot) ) * r_slot
				var seg = ( end - start ) / div
				var dir = seg.normalized()
				var perp = Vector3( dir.z, 0, -dir.x )
				
				var hh = 0
				for h in range(1,div,2):
					
					var s = add_slot( slot, id_slot )
					s.translation = start + seg * h
					if hh % 2 == 0:
						s.translation += perp * (radius+gap) * (1 - sin(PI/3))
					else:
						s.translation -= perp * (radius+gap) * (1 - sin(PI/3))
					s.rotation = Vector3( 0, rot_slot, 0 )
					id_slot += 1
					hh += 1
					rot_slot += PI / 3
				
				if lvl > 0:
					
					start = Vector3( cos(a_button), 0, sin(a_button) ) * r_button
					a_button += PI / 3
					end = Vector3( cos(a_button), 0, sin(a_button) ) * r_button
					seg = ( end - start ) / lvl
					for h in range(0,lvl):
						var b = add_button( button, lvl, i, h )
						var pos = start + seg * h
						b.translation = pos
						var r = Vector2( pos.x, -pos.z )
						b.rotation = Vector3( 0, r.angle(), 0 )
					
		
		r_button += radius * sin(PI/3) + gap
		r_slot += radius * sin(PI/3) + gap

func _generate(g):
	generate = false
	if g:
		do_generate()

func reset( mat_id = null ):
	for b in $buttons.get_children():
		var data = grid[b.level][b.corner][b.id]
		if ( mat_id == null and data.mat != null ) or ( mat_id != null and data.mat == mat_button_list[ mat_id ] ):
			data.mat = null
			b.pastille_material( mat_button_default )
			var button = get_node( data.path )
			button.decoy_enable( false )

func reset_button( b ):
	var data = grid[b.level][b.corner][b.id]
	if data.mat == null:
		b.pastille_material( mat_button_default )
	else:
		b.pastille_material( data.mat )

func check_matid( mat_id ):
	if mat_id  < 0 or mat_id > len( mat_button_list ):
		return DEFAULT_MATID
	return mat_id

func hover_button( b, mat_id = 0 ):
	
	mat_id = check_matid( mat_id )
	
	if b == null and prev_hover != null:
		reset_button( prev_hover )
		prev_hover = null
		return false
	
	if $buttons == null:
		return
	if not b in $buttons.get_children():
		return
	
	# getting button in grid
	var data = grid[b.level][b.corner][b.id]
	var button = get_node( data.path )
	
	if prev_hover != null and prev_hover != button:
		reset_button( prev_hover )
	button.pastille_material( mat_button_list[ mat_id ] )
	prev_hover = button
	
	return true

func click_button( b, mat_id = 0 ):
	
	if b == null:
		return
	
	mat_id = check_matid( mat_id )
	
	var data = grid[b.level][b.corner][b.id]
	var button = get_node( data.path )
	
	if data.mat != mat_button_list[ mat_id ]:
		data.mat = mat_button_list[ mat_id ]
		button.pastille_material( data.mat )
		button.decoy_material( decoy_mats[ mat_id ] )
		button.decoy_enable( true )
	else:
		data.mat = null
		button.decoy_enable( false )

func add_spatial( blaz ):	
	if get_node( blaz ):
		return
	var s = Spatial.new()
	s.name = blaz
	add_child(s)
	s.set_owner(get_tree().get_edited_scene_root())

func add_hexa( tmpl, id ):
	var sprite = tmpl.duplicate()
	sprite.name = "hexa_" + str(id)
	add_child(sprite)
	sprite.set_owner(get_tree().get_edited_scene_root())
	return sprite

func add_slot( tmpl, id ):
	var s = tmpl.instance()
	s.scale = Vector3( radius2, height2, radius2 )
	s.name = "slot_" + str(id)
	add_spatial( "slots" )
	$slots.add_child(s)
	s.set_owner(get_tree().get_edited_scene_root())
	return s
	
func add_button( tmpl, lvl, corner, id ):
	
	var s = tmpl.instance( PackedScene.GEN_EDIT_STATE_MAIN )
	s.scale_pastille( Vector3( radius3, height3, radius3 ) )
	s.name = "b_" + str(lvl) + "-" + str(corner) + "-" + str(id)
	# registering position info
	s.level = lvl
	s.corner = corner
	s.id = id
	
	add_spatial( "buttons" )
	$buttons.add_child(s)
	s.set_owner(get_tree().get_edited_scene_root())
	
	# storing in grid
	store_button( s )
	
	return s

func store_button( button ):
	
	# storing in grid
	while len( grid ) <= button.level:
		grid.append([])
	while len(grid[button.level]) <= button.corner:
		grid[button.level].append([])
	while len(grid[button.level][button.corner]) <= button.id:
		grid[button.level][button.corner].append(null)
	
	# button data dictionary!
	grid[ button.level ][ button.corner ][ button.id ] = {
		'path': 'buttons/' + button.name,
		'mat': null,
		'albedo': null
		} 

func get_mat_albedo( mat_id ):
	mat_id = check_matid( mat_id )
	return mat_button_list[ mat_id ].get_shader_param( 'albedo' )

func set_mat_albedo( mat_id, c ):
	if mat_id  < 0 or mat_id > len( mat_button_list ):
		return
	mat_button_list[ mat_id ].set_shader_param( 'albedo', c )
	decoy_mats[ mat_id ].set_shader_param( 'albedo', c )
	decoy_mats[ mat_id ].next_pass.set_shader_param( 'albedo', Color(1,1,1,2) - c )
	pass

func _ready():
	set_mat_albedo(0,Color(1,1,1,1))
	set_mat_albedo(1,Color(0,0,0,1))
	pass

# warning-ignore:unused_argument
func _process(delta):
	pass

func get_selected_positions():
	
	var out = []
	var mnum = len( mat_button_list )
	for m in mat_button_list:
		out.append( [] )
	
	for b in $buttons.get_children():
		var data = grid[b.level][b.corner][b.id]
		for i in range( 0, mnum ):
			if data.mat == mat_button_list[i]:
				out[i].append( b.global_transform.origin )
	
	return out
