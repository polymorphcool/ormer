extends Node2D

export (bool) var _synchronise_style = false setget synchronise_style

func synchronise_style( b ):
	_synchronise_style = false
	if _synchronise_style:
		var tmpl = load( "res://models/2d/hexa_button.tscn" ).instance()
		for c in get_children():
			if c is StaticBody2D:
				c.cp_styles( tmpl )

func _ready():
	for c in get_children():
		if c is StaticBody2D:
			c.connect( "hexa_pressed", self, "hexa_pressed" )
	pass

func _process(delta):
	pass

func hexa_pressed( button ):
	
	print( button )
	
	if button == $hbutton:
		$hbutton2.disable( not $hbutton2.disabled )
		if $hbutton2.disabled:
			$hbutton2.text('OFF')
		else:
			$hbutton2.text('ON')
		$hbutton3.select( not $hbutton3.selected )
		$hbutton4.select( not $hbutton4.selected )
		$hbutton5.visible = !$hbutton5.visible
		if $hbutton5.visible:
			$hbutton5.restart()
	
	if $hbutton3.selected:
		$hbutton3.text('SLCTed')
	else:
		$hbutton3.text('SLCT')
	
	if $hbutton4.selected:
		$hbutton4.text('SLCTed')
	else:
		$hbutton4.text('SLCT')
