extends RigidBody

export (bool) var adjust_boxes = false
export (float,0,1) var scaling = 1

func _ready():
	pass # Replace with function body.

# warning-ignore:unused_argument
func _process(delta):
	
	if adjust_boxes:
		adjust_boxes = false
		var els = Vector3(scaling,scaling,scaling)
		var boxes = [$box0,$box1,$box2]
		var ref = $box0.transform
		var q = Quat( Vector3(0,1,0), TAU / 3 )
		var rot = Transform( Basis(q), Vector3() )
		for i in range( 0, 3 ):
			boxes[i].transform = ref
			boxes[i].scale = els
			ref = rot * ref
		$sphere.scale = els
		$basis.scale = els
		$ball.scale = els
