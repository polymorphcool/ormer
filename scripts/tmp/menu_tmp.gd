extends Node2D

export (bool) var _synchronise_style = false setget synchronise_style

func synchronise_style( b ):
	_synchronise_style = false
	var tmpl = get_child( 0 )
	for c in get_children():
		if c == tmpl:
			continue
		c.cp_style( tmpl )

func _ready():
	for c in get_children():
		if c is StaticBody2D:
			c.connect( "hexa_pressed", self, "hexa_pressed" )
	pass

func _process(delta):
	pass

func show_edit( enabled ):
	if enabled:
		
		$play.disable( true )
		$edit.disable( true )
		$edit_ok.visible = true
		$edit_cancel.visible = true
		$edit_p1.visible = true
		$edit_p2.visible = true
		$edit_ok.restart()
		$edit_cancel.restart()
		$edit_p1.restart()
		$edit_p2.restart()
	else:
		$play.disable( false )
		$edit.disable( false )
		$edit_ok.visible = false
		$edit_cancel.visible = false
		$edit_p1.visible = false
		$edit_p2.visible = false
		$p1_color.visible = false
		$p2_color.visible = false

func show_p1( enabled ):
	if enabled:
		$p1_color.visible = true
		$p1_color.restart()
		$p2_color.visible = false
	else:
		$p1_color.visible = false

func show_p2( enabled ):
	if enabled:
		$p2_color.visible = true
		$p2_color.restart()
		$p1_color.visible = false
	else:
		$p2_color.visible = false

func hexa_pressed( button ):
	print( button )
	if button == $edit:
		show_edit( true )
	elif button == $edit_ok or button == $edit_cancel:
		show_edit( false )
	elif button == $edit_p1:
		show_p1( true )
	elif button == $edit_p2:
		show_p2( true )
