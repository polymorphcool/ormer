extends Spatial

const DEFAULT_MATID = 0
const slot_path = "res://models/slot.tscn"
const pastille_path = "res://models/pastille.tscn"

onready var gm = get_node("/root/grid_manager")
onready var pm = get_node("/root/player_manager")

export (int,0,20) var depth = 2
export (float,0,10) var radius = 20
export (float,0,10) var gap = 5
export (float,0,10) var radius2 = 5
export (float,0,10) var height2 = 5
export (float,0,10) var radius3 = 5
export (float,0,10) var height3 = 5
export (bool) var generate = false setget _generate

var slot = null
var pastille = null
var prev_hover = null

var default_pastille_material = null
var default_decoy_material = null

func synchronise():
	
	if gm.active_grid == null:
		return
	
	if depth != gm.active_grid.depth:
		print( "generating a new board of depth ", depth )
		depth = gm.active_grid.depth
		do_generate()
	
	# synchronising display
	for i in range( 0, len(gm.active_grid.cells) ):
		var pastille = $pastilles.get_child( i )
		var cell = gm.get_cell_by_id( gm.active_grid, i )
		pastille.selected = cell.player != -1
		pastille_set_player( pastille, cell.player )
	
	store_pastilles()

func do_generate():
	
	while( get_child_count() > 0 ):
		remove_child( get_child(0) )

	slot = load(slot_path)
	pastille = load(pastille_path)
	
	var bi = pastille.instance()
	bi.scale_pastille( Vector3( radius3, height3, radius3 ) )
	var pb = PackedScene.new()
	var result = pb.pack(bi)
	if result == OK:
# warning-ignore:return_value_discarded
		ResourceSaver.save(pastille_path, pb)
		pastille = load(pastille_path)
	
#	grid = []
	
	var r_slot = ( radius * sin(PI/3) + gap ) * 0.5
	var r_pastille = 0
	var id_slot = 0
	
	for lvl in range( 0, depth + 1 ):
		
		if lvl > -1:
			
			var a_slot = PI / 6
			var a_pastille = PI / 6
			var rot_slot = 0
			
			if lvl == 0:
				add_pastille( pastille )
			
			for i in range( 0, 6 ):
				
				var div = lvl * 4 + 2
				var start = Vector3( cos(a_slot), 0, sin(a_slot) ) * r_slot
				a_slot += PI / 3
				var end = Vector3( cos(a_slot), 0, sin(a_slot) ) * r_slot
				var seg = ( end - start ) / div
				var dir = seg.normalized()
				var perp = Vector3( dir.z, 0, -dir.x )
				
				var hh = 0
				for h in range(1,div,2):
					
					var s = add_slot( slot, id_slot )
					s.translation = start + seg * h
					if hh % 2 == 0:
						s.translation += perp * (radius+gap) * (1 - sin(PI/3))
					else:
						s.translation -= perp * (radius+gap) * (1 - sin(PI/3))
					s.rotation = Vector3( 0, rot_slot, 0 )
					id_slot += 1
					hh += 1
					rot_slot += PI / 3
				
				if lvl > 0:
					
					start = Vector3( cos(a_pastille), 0, sin(a_pastille) ) * r_pastille
					a_pastille += PI / 3
					end = Vector3( cos(a_pastille), 0, sin(a_pastille) ) * r_pastille
					seg = ( end - start ) / lvl
					for h in range(0,lvl):
						var b = add_pastille( pastille )
						var pos = start + seg * h
						b.translation = pos
						var r = Vector2( pos.x, -pos.z )
						b.rotation = Vector3( 0, r.angle(), 0 )
					
		
		r_pastille += radius * sin(PI/3) + gap
		r_slot += radius * sin(PI/3) + gap

func _generate(g):
	generate = false
	if g:
		do_generate()

func get_pastille_id( b ):

	var i = 0
	for btn in $pastilles.get_children():
		if b == btn:
			return i
		i += 1
	
	return -1

func show_pastilles( b ):
	for btn in $pastilles.get_children():
		btn.show_pastille( b )

func show_decoys( b ):
	if gm.active_grid == null or gm.active_grid.cell_num == 0:
		return
	for i in range( 0, $pastilles.get_child_count() ):
		var btn = $pastilles.get_child( i )
		var cell = gm.get_cell_by_id( gm.active_grid, i )
		if cell.player != -1:
			pastille_set_player( btn, cell.player )
			btn.show_decoy( b )

func pastille_hover( b ):
	
	if gm.active_grid == null:
		return
	
	if prev_hover == b:
		return
	
	if prev_hover != null:
		prev_hover.pastille_material( default_pastille_material )
	prev_hover = null
	if b == null: 
		return
	
	prev_hover = b
	
	var bid = get_pastille_id( b )
	if bid == -1:
		return
	
	var cell = gm.get_cell_by_id( gm.active_grid, bid )
	var player = pm.get_player( pm.current_player )
	if player == null:
		return
	if b.pastille_material() == player.mat_pastille:
		return
	b.pastille_material( player.mat_pastille )

func pastille_click( b ):
	
	if gm.active_grid == null or b == null:
		return
	
	var bid = get_pastille_id( b )
	if bid == -1:
		return
	
	var cell = gm.get_cell_by_id( gm.active_grid, bid )
	var player = pm.get_player( pm.current_player )
	if player == null:
		return
	
	if cell.player == pm.current_player:
		b.selected = true
		pastille_set_player( b, pm.current_player )
	else:
		b.selected = false
		pastille_set_player( b, pm.current_player )

func pastille_set_player( btn, pid ):
	
	if btn.selected and pid != -1:
		var player = pm.get_player( pid )
		btn.decoy_material( player.mat_decoy )
	btn.pastille_material( default_pastille_material )
	btn.decoy_enable( btn.selected )

func add_spatial( blaz ):	
	if get_node( blaz ):
		return
	var s = Spatial.new()
	s.name = blaz
	add_child(s)
	s.set_owner(get_tree().get_edited_scene_root())

func add_hexa( tmpl, id ):
	var sprite = tmpl.duplicate()
	sprite.name = "hexa_" + str(id)
	add_child(sprite)
	sprite.set_owner(get_tree().get_edited_scene_root())
	return sprite

func add_slot( tmpl, id ):
	
	add_spatial( "slots" )
	
	var s = tmpl.instance()
	s.scale = Vector3( radius2, height2, radius2 )
	s.name = str($slots.get_child_count())
	while len( s.name ) < 3:
		s.name = '0' + s.name
	s.name = "s" + s.name
	$slots.add_child(s)
	s.set_owner(get_tree().get_edited_scene_root())
	return s
	
func add_pastille( tmpl ):
	
	add_spatial( "pastilles" )
	
	var s = tmpl.instance( PackedScene.GEN_EDIT_STATE_MAIN )
	s.scale_pastille( Vector3( radius3, height3, radius3 ) )
	s.name = str($pastilles.get_child_count())
	while len( s.name ) < 3:
		s.name = '0' + s.name
	s.name = "b" + s.name
	
	$pastilles.add_child(s)
	s.set_owner(get_tree().get_edited_scene_root())
	
	return s

func store_pastilles():
	
	if gm.active_grid == null:
		return
	
	for bi in range( 0, $pastilles.get_child_count() ):
		var b = $pastilles.get_child( bi )
		var cell = gm.get_cell_by_id( gm.active_grid, bi )
		cell.pastille = b

func _ready():
	var btn = load(pastille_path).instance()
	default_pastille_material = btn.pastille_material()
	default_decoy_material = btn.decoy_material()
	show_pastilles( false )
	show_decoys( false )

# warning-ignore:unused_argument
func _process(delta):
	pass
